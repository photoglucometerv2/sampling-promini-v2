#include <Wire.h> //ADC
#include <Adafruit_ADS1X15.h> //ADC
#include <AltSoftSerial.h> //Bluetooth
#include <avr/io.h> //Timer
#include <avr/interrupt.h> //Timer
#include <SoftwareSerial.h>

SoftwareSerial bluetooth(8,9); // TX, RX
Adafruit_ADS1115 adsppgf; //ADC
Adafruit_ADS1115 adsppge; //ADC
Adafruit_ADS1115 adsecg; //ADC
//Adafruit_ADS1115 ads1; //ADC
//Adafruit_ADS1115 ads2; //ADC

//AltSoftSerial BTserial; //Bluetooth

//int16_t ppg1_16bits;
//int16_t ppg2_16bits;
int16_t adc0;
int16_t adc1;
int16_t adc2;


volatile bool timeToSample = false;

void setup(void)
{
  //---------- ADC ----------

  adsppgf.begin(0x48); /*ADDR = GND*/
  adsppge.begin(0x49); /*ADDR = VDD*/
  adsecg.begin(0x4a); /*ADDR = SDA*/
//  ads1.begin(0x48);
//  ads2.begin(0x49);
//  ads.setSPS(ADS1115_DR_860SPS);
  adsppge.startADCReading(MUX_BY_CHANNEL[0], /*continuous=*/true);
  adsppgf.startADCReading(MUX_BY_CHANNEL[0], /*continuous=*/true);
  adsecg.startADCReading(MUX_BY_CHANNEL[0], /*continuous=*/true);
//  ads1.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_0_1, /*continuous=*/true);
//  ads2.startADCReading(ADS1X15_REG_CONFIG_MUX_DIFF_0_1, /*continuous=*/true);

  //---------- Bluetooth ----------
  //BTserial.begin(115200);
  bluetooth.begin(115200);
  Serial.begin(9600);


  //---------- Interrupt ----------

  //stop interrupts
  cli();
  
  //TCCR2A = COM2A1, COM2A0, COM2B1, COM2B0, -, -, WGM21, WGM20
  TCCR2A = 0b00000010; //WGM01:0 = 10 -> CTC mode of operation
  
  //TCCR2B = FOC2A, FOC2B, -, -, WGM22, CS22, CS21, CS20
  TCCR2B = 0b00000111; //CS02:0 = 111 -> prescaler 1024
  
  //initialize counter value to 0
  TCNT2  = 0;
  
  // set compare match register for 100 Hz increments
  OCR2A = 155;// = (16*10^6) / (100*1024) - 1 (must be <256)

  // enable timer compare interrupt
  TIMSK2 = 0b00000010;

  //allow interrupts
  sei();
}

//interrupt de timer 2
ISR (TIMER2_COMPA_vect)
{
  timeToSample = true;
}

void loop(void)
{
  if(timeToSample){
    adc0 = adsppgf.getLastConversionResults();
    adc1 = adsppge.getLastConversionResults();
    adc2 = adsecg.getLastConversionResults();
//    adc0=ads.readADC_SingleEnded(0);
//    adc1=ads.readADC_SingleEnded(1);
//    adc2=ads.readADC_SingleEnded(2);

//    ppg1_16bits = ads1.getLastConversionResults();
//    ppg2_16bits = ads2.getLastConversionResults();

    timeToSample = false;
    bluetooth.print(adc0);
    bluetooth.print(",");
    bluetooth.print(adc1);
    bluetooth.print(";");
    bluetooth.println(adc2);
    Serial.println(adc0);

//    Serial.print(ppg1_16bits);
//    Serial.print(",");
//    Serial.println(ppg2_16bits);
//    bluetooth.print(ppg1_16bits);
//    bluetooth.print(",");
//    bluetooth.println(ppg2_16bits);

  }
}
